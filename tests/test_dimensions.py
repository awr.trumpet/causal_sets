import pytest

import causal_sets as cs
from causal_sets.algorithms._dimensions import ordering_fraction


@pytest.mark.parametrize("dimension, expected_ordering_fraction", [
    (2, 0.25)
])
def test_ordering_fraction_of_known_dimension_is_expected_fraction(
    dimension: int, expected_ordering_fraction: float):
    assert ordering_fraction(dimension) == expected_ordering_fraction


class TestMyrheimMeyerDimension:
    def test_mm_dimension_of_an_empty_causal_set_is_zero(self):
        assert cs.myrheim_meyer_dimension(cs.CausalSet()) == 0

    def test_mm_dimension_of_causal_set_with_no_relations_is_zero(self):
        assert cs.myrheim_meyer_dimension(
            cs.CausalSet.from_elements([0, 1, 2, 3])) == 0

    def test_mm_dimension_of_set_with_relation_between_two_elements_is_two(
            self):
        assert cs.myrheim_meyer_dimension(
            cs.CausalSet.from_elements(relations=[(0, 1)])) == pytest.approx(2)
