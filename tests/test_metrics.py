from typing import Iterable

import numpy as np
import numpy.testing
import numpy.typing as npt
import pytest

from causal_sets import MinkowskiMetric
from causal_sets._metrics import Metric

Point = npt.NDArray[np.floating]
CausalMatrix = npt.NDArray[np.bool_]


class TestMinkowskiMetric:
    @pytest.mark.parametrize("point", [
        np.array([0, 0]),
        np.array([123, 456]),
        np.array([1, 2, 3, 4])
    ])
    def test_distance_between_the_same_point_is_zero(self, point: Point):
        metric = MinkowskiMetric(len(point))
        assert metric.distance_between(point, point) == 0

    @pytest.mark.parametrize("point1, point2, expected_distance", [
        (np.array([0, 0]), np.array([1, 0]), -1),
        (np.array([0, 0]), np.array([0, 1]), 1),
        (np.array([0, 0]), np.array([1, 1]), 0)
    ])
    def test_distance_between_two_differnet_points_is_expected_distance(
        self,
        point1: Point,
        point2: Point,
        expected_distance: float
    ):
        metric = MinkowskiMetric(len(point1))
        assert metric.distance_between(point1, point2) == expected_distance

    @pytest.mark.parametrize("metric", [
        MinkowskiMetric(2),
        MinkowskiMetric(3),
        MinkowskiMetric(4)
    ])
    @pytest.mark.parametrize("num_elements", (2, 3, 4, 10, 100))
    def test_causal_matrix_has_correct_shape(
        self,
        metric: Metric,
        num_elements: int
    ):
        coordinates = np.zeros((num_elements, metric.num_dimensions))
        assert metric.causal_matrix(coordinates).shape == (
            num_elements, num_elements)

    @pytest.mark.parametrize("coordinates, expected_causal_matrix", [
        (np.array([[0, 0], [1, 0]]), np.array(
            [[False, True], [False, False]])),
        (np.array([[0, 0], [0, 1]]), np.array(
            [[False, False], [False, False]])),
        (np.array([[1, 0], [0, 0]]), np.array([[False, False], [True, False]]))
    ])
    def test_causal_matrix_on_known_coordinates_is_expected_matrix(
        self,
        coordinates: Iterable[Point],
        expected_causal_matrix: CausalMatrix
    ):
        metric = MinkowskiMetric(coordinates.shape[1])
        numpy.testing.assert_array_equal(
            metric.causal_matrix(coordinates), expected_causal_matrix)
