from typing import Iterable, Tuple

import networkx as nx
import pytest

from causal_sets import CausalSet


@pytest.fixture
def empty_causal_set() -> CausalSet:
    return CausalSet()


class TestErrors:
    def test_error_is_raised_if_passed_graph_is_not_a_directed_graph(self):
        with pytest.raises(TypeError, match="Graph must be a directed "
                                            "NetworkX graph"):
            CausalSet(nx.Graph())


class TestElementAndEdgeMembership:
    def test_element_not_in_graph_is_not_in_causal_set(
        self,
        empty_causal_set: CausalSet
    ):
        assert 0 not in empty_causal_set

    @pytest.mark.parametrize("causal_relation", [
        (0, 1),
        [0, 1],
    ])
    def test_causal_relation_not_in_graph_is_not_in_causal_set(
        self,
        causal_relation: Iterable[int],
        empty_causal_set: CausalSet
    ):
        assert causal_relation not in empty_causal_set

    def test_causal_relation_in_graph_is_directed(self):
        graph = nx.DiGraph()
        graph.add_edge(0, 1)
        causal_set = CausalSet(graph)
        assert (0, 1) in causal_set
        assert (1, 0) not in causal_set

    def test_element_is_in_causal_set_if_it_in_the_graph(self):
        graph = nx.DiGraph()
        graph.add_node(0)
        causal_set = CausalSet(graph)
        assert 0 in causal_set

    def test_causal_relation_is_in_causal_set_if_it_is_in_the_graph(self):
        graph = nx.DiGraph()
        graph.add_edge(0, 1)
        causal_set = CausalSet(graph)
        assert (0, 1) in causal_set


class TestAlternativeConstructors:
    def test_causal_set_from_only_elements_has_all_element_in_set(self):
        elements = [0, 1, 2]
        causal_set = CausalSet.from_elements(elements)
        for element in elements:
            assert element in causal_set

    def test_causal_set_from_only_elements_has_no_relations(self):
        assert len(CausalSet.from_elements([0]).relations) == 0

    def test_causal_set_from_relations_has_all_elements_in_set(self):
        causal_set = CausalSet.from_elements(relations=[(0, 1)])
        assert 0 in causal_set
        assert 1 in causal_set

    def test_causal_set_from_relations_has_all_relations_in_set(self):
        relations = [(0, 1), (3, 4)]
        causal_set = CausalSet.from_elements(relations=relations)
        for relation in relations:
            assert relation in causal_set


class TestAddingElementsAndRelations:
    def test_inserting_element_with_no_causal_adds_it_to_set(
        self,
        empty_causal_set: CausalSet
    ):
        empty_causal_set.insert_element(0)
        assert 0 in empty_causal_set

    def test_inserting_element_with_past_relation_adds_relation_to_set(
        self,
        empty_causal_set: CausalSet
    ):
        empty_causal_set.insert_element(0, causal_past=[1])
        assert (1, 0) in empty_causal_set

    def test_inserting_element_with_future_relation_adds_relation_to_set(
        self,
        empty_causal_set: CausalSet
    ):
        empty_causal_set.insert_element(0, causal_future=[1])
        assert (0, 1) in empty_causal_set

    def test_adding_from_causal_relations_add_elements_to_to_set(
        self,
        empty_causal_set: CausalSet
    ):
        empty_causal_set.add_from_relations([(0, 1)])
        assert 0 in empty_causal_set
        assert 1 in empty_causal_set

    @pytest.mark.parametrize("causal_relations", [
        [(0, 1)],
        [(1, 0)],
        [(0, 1), (1, 2)]
    ])
    def test_adding_from_causal_relations_adds_relations_to_set(
        self,
        empty_causal_set: CausalSet,
        causal_relations: Iterable[Tuple[int, int]]
    ):
        empty_causal_set.add_from_relations(causal_relations)
        for relation in causal_relations:
            assert relation in empty_causal_set


class TestRemovingElementsAndRelations:
    def test_removing_single_element_takes_element_out_of_set(self):
        causal_set = CausalSet.from_elements([0])
        causal_set.remove_elements(0)
        assert 0 not in causal_set

    @pytest.mark.parametrize("elements", [
        (0, 1, 2),
        [0, 1, 2]
    ])
    def test_removing_iterable_of_elements_takes_all_out_of_set(
            self, elements: Iterable[int]):
        causal_set = CausalSet.from_elements(elements)
        causal_set.remove_elements(elements)
        assert all(element not in causal_set for element in elements)

    def test_removing_element_only_removes_that_element(self):
        causal_set = CausalSet.from_elements([0, 1])
        causal_set.remove_elements(0)
        assert 1 in causal_set

    def test_removing_element_also_removes_relations_containing_that_element(
            self):
        causal_set = CausalSet.from_elements(relations=[(0, 1)])
        causal_set.remove_elements(0)
        assert (0, 1) not in causal_set

    def test_removing_single_relation_takes_relation_out_of_set(self):
        relation = (0, 1)
        causal_set = CausalSet.from_elements(relations=[relation])
        causal_set.remove_relations(relation)
        assert relation not in causal_set

    def test_removing_iterable_of_relations_takes_all_out_of_set(self):
        relations = [(0, 1), (1, 2)]
        causal_set = CausalSet.from_elements(relations=relations)
        causal_set.remove_relations(relations)
        for relation in relations:
            assert relation not in causal_set

    def test_removing_relation_only_removes_specified_relation(self):
        relations = [(0, 1), (1, 2)]
        causal_set = CausalSet.from_elements(relations=relations)
        causal_set.remove_relations(relations[0])
        assert relations[1] in causal_set


class TestCausalInterval:
    def test_error_is_raised_if_past_element_does_not_preceed_future_element(
            self):
        causal_set = CausalSet.from_elements(relations=[(0, 1)])
        with pytest.raises(ValueError, match="Past element must be in the "
                                             "causal past of future element."):
            causal_set.interval(1, 0)

    def test_error_is_raised_if_no_relation_exists_between__elements(self):
        causal_set = CausalSet.from_elements([0, 1])
        with pytest.raises(ValueError, match="Past element must be in the "
                                             "causal past of future element."):
            causal_set.interval(0, 1)

    def test_interval_of_set_with_one_relation_is_that_set(self):
        causal_set = CausalSet.from_elements(relations=[(0, 1)])
        interval = causal_set.interval(0, 1)
        assert interval.elements == causal_set.elements
        assert interval.relations == causal_set.relations

    def test_complete_diamond_interval_of_planar_set_has_expected_relations(
            self):
        r"""
        6 7 8
        |X|X|
        3 4 5
        |X|X|
        0 1 2
        """
        causal_set = CausalSet.from_elements(relations=[
            (0, 3), (0, 6), (0, 4), (0, 7), (0, 8),
            (1, 3), (1, 4), (1, 5), (1, 6), (1, 7), (1, 8),
            (2, 4), (2, 6), (2, 7), (2, 5), (2, 8),
            (3, 6), (3, 7), (4, 6), (4, 7), (4, 8), (5, 7), (5, 8)])
        interval = causal_set.interval(1, 7)
        assert set(interval.relations) == {
            (1, 3), (1, 4), (1, 5), (1, 7), (3, 7), (4, 7), (5, 7)}

    def test_incomplete_diamond_interval_of_planar_set_has_expected_elements(
            self):
        r"""
        6 7 8
        |\|/|
        3 4 5
        |\|/|
        0 1 2
        """
        causal_set = CausalSet.from_elements(relations=[
            (0, 3), (0, 6),
            (1, 3), (1, 6), (1, 4), (1, 7), (1, 5), (1, 8),
            (2, 5), (2, 8),
            (3, 6), (4, 6), (4, 7), (4, 8), (5, 8)])
        interval = causal_set.interval(1, 7)
        assert set(interval.elements) == {1, 4, 7}
