from __future__ import annotations

import collections.abc
from itertools import chain
from typing import Generic, Hashable, Iterable, Tuple, Type, TypeVar

import networkx as nx
import numpy as np

from causal_sets._metrics import Metric

T = TypeVar("T", bound=Hashable)
CS = TypeVar("CS", bound="CausalSet")


class CausalSet(Generic[T]):
    def __init__(self, graph: nx.DiGraph | None = None):
        graph = graph if graph is not None else nx.DiGraph()
        if not isinstance(graph, nx.DiGraph):
            raise TypeError("Graph must be a directed NetworkX graph.")
        self._graph = graph

    @classmethod
    def from_elements(
        cls,
        elements: Iterable[T] = (),
        relations: Iterable[Tuple[T, T]] = ()
    ) -> CausalSet:
        """Create a Causal Set instance from known elements and relations
        between elements. If an element exists in relations but not in
        elements then an element is automatically added to the set.

        Parameters
        ----------
        elements : Iterable[T], optional
            The elements of this causal set. If an element doesn't have any
            relations then it must be added with this argument. By default ().
        relations : Iterable[Tuple[T, T]], optional
            The relations between elements. If an element is not in the
            elements argument but is in a relation the element is added. By
            default ().

        Returns
        -------
        CausalSet
            A causal set containing all elements and relations.
        """
        graph = nx.DiGraph(relations)
        graph.add_nodes_from(elements)
        return cls(graph)

    @property
    def graph(self) -> nx.DiGraph:
        """Get a graph view of the graph which defines this causal set."""
        return nx.subgraph_view(self._graph)

    @property
    def elements(self) -> nx.NodeView:
        """Get all the elements in this causal set."""
        return self._graph.nodes

    @property
    def relations(self) -> nx.EdgeView:
        """Get all relations in this causal set."""
        return self._graph.edges

    def add_from_relations(self, causal_relations: Iterable[Tuple[T, T]]):
        """Add elements and relations to this causal set.

        Parameters
        ----------
        causal_relations : Iterable[Tuple[T, T]]
            The iterable of relations to add. Elements are added to this set
            if they don't already exist in this set.
        """
        self._graph.add_edges_from(causal_relations)

    def insert_element(
        self,
        element: T,
        causal_past: Iterable[T] = (),
        causal_future: Iterable[T] = ()
    ):
        """Insert a single element into this causal set where the elements in
        the causal future and past are know. For single elements this is more
        convenient than calling `add_from_relations`.

        Parameters
        ----------
        element : T
            The element to add to this causal set. If it is already in the set
            a new element is not created.
        causal_past : Iterable[T], optional
            All the elements in the causal past of this point. Any element in
            the iterable not in the set is added to the set. By default ().
        causal_future : Iterable[T], optional
            All the elements in the causal future of this point. Any element
            in the iterable not in the set is added to the set. By default ().
        """
        self._graph.add_node(element)
        self._graph.add_edges_from(chain(
            ((past_element, element) for past_element in causal_past),
            ((element, future_element) for future_element in causal_future)
        ))

    def remove_elements(self, elements: T | Iterable[T]):
        """Remove single element or iterable of elements from this set. This
        will also remove all relations on those elements.

        Parameters
        ----------
        elements : T | Iterable[T]
            A single element or iterable of elements to remove.
        """
        if isinstance(elements, collections.abc.Iterable):
            self._graph.remove_nodes_from(elements)
        else:
            self._graph.remove_node(elements)

    def remove_relations(self, relations: Tuple[T, T] | Iterable[Tuple[T, T]]):
        """Remove a single relation or iterable of relations from this set.

        Parameters
        ----------
        relations : Tuple[T, T] | Iterable[Tuple[T, T]]
            A single relation or an iterable of relations to remove.
        """
        try:
            self._graph.remove_edges_from(relations)
        except TypeError:
            self._graph.remove_edge(*relations)

    def interval(self, past_element: T, future_element: T) -> CausalSet:
        """Create a causal interval between two elements where the causal
        relations in the interval are inherited from the relations in this set.

        Parameters
        ----------
        past_element : T
            The past element of the interval.
        future_element : T
            The future element of the interval.

        Returns
        -------
        CausalSet
            A causal set with all elements and relations in the causal future
            of the past point and the causal past of the future point.

        Raises
        ------
        ValueError
            If the past element and future element are not causally related.
        """
        future_of_past_element = set(self._graph.successors(past_element))
        past_of_future_element = self._graph.predecessors(future_element)
        if future_element not in future_of_past_element:
            raise ValueError("Past element must be in the causal past of "
                             "future element.")
        interval_nodes = {past_element, future_element}.union(
            future_of_past_element.intersection(past_of_future_element))
        return CausalSet(nx.DiGraph(nx.subgraph_view(
            self._graph,
            filter_node=lambda u: u in interval_nodes,
            filter_edge=lambda u, v: {u, v}.issubset(interval_nodes))))

    @classmethod
    def from_sprinkling(
        cls: Type[CS],
        unit_density: int,
        metric: Metric,
        seed: int | None = None
    ) -> CS:
        """Generate a causal set using a random Poisson process. The causal
        order is calculated by sprinkling points into a continuum and
        determining the causal relations using the metric of the continuum.

        Parameters
        ----------
        unit_density : int
            The expected number of points in a unit volume of spacetime.
        metric : Metric
            The metric of the continuum spacetime in order to determine the
            causal order.
        seed : int | None, optional
            The seed for random, by default None
        """
        rng = np.random.default_rng(seed=seed)
        num_elements = rng.poisson(unit_density)
        coordinates = rng.random((num_elements, metric.num_dimensions))
        return cls(nx.DiGraph(metric.causal_matrix(coordinates)))

    def __contains__(self, element: T | Tuple[T, T]) -> bool:
        if isinstance(element, collections.abc.Iterable):
            return self._graph.has_edge(*element)
        return self._graph.has_node(element)
