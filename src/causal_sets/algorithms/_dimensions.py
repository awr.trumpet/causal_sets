from fractions import Fraction
import math

from scipy.constants import golden
from scipy.special import gamma

from .._causal_set import CausalSet


def ordering_fraction(dimension: float) -> float:
    """Return the ordering fraction of the given dimension."""
    return ((gamma(dimension + 1) * gamma(dimension / 2)) /
            (4 * gamma(3 * dimension / 2)))


def myrheim_meyer_dimension(
    causal_set: CausalSet,
    rel_tol: float = 1e-7,
    abs_tol: float = 0
) -> float:
    """Calculate the Myrheim-Meyer dimension of a causal set to within a given
    tolerance. This converges to a dimension by picking a dimension,
    calculating the ordering fraction and then

    Parameters
    ----------
    causal_set : CausalSet
        The causal set to estimate the dimension of.
    rel_tol : float, optional
        The relative tolerance between the estimate of the ordering fraction
        and the causal relations ratio, by default 1e-7
    abs_tol : float, optional
        The absolute tolerance between the estimate of the ordering fraction
        and the causal relations ratio, by default 0

    Returns
    -------
    float
    """
    num_elements = len(causal_set.elements)
    num_relations = len(causal_set.relations)
    if num_elements == 0 or num_relations == 0:
        return 0

    ratio = Fraction(num_relations, num_elements ** 2)
    min_dimension, max_dimension = 0, 100
    dimension_estimate = min_dimension + \
        (max_dimension - min_dimension) / golden
    ordering_fraction_guess = ordering_fraction(dimension_estimate)
    while not math.isclose(
            ordering_fraction_guess, ratio, rel_tol=rel_tol, abs_tol=abs_tol):
        if ordering_fraction_guess < ratio:
            max_dimension = dimension_estimate
        else:
            min_dimension = dimension_estimate
        dimension_estimate = min_dimension + \
            (max_dimension - min_dimension) / golden
        ordering_fraction_guess = ordering_fraction(dimension_estimate)
    return dimension_estimate
