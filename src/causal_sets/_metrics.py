from abc import ABC, abstractmethod
from itertools import permutations

import numpy as np
import numpy.typing as npt


class Metric(ABC):
    def __init__(self, num_dimensions: int):
        self._num_dimensions = num_dimensions

    @property
    def num_dimensions(self) -> int:
        """Get the number of dimensions this metric allows for."""
        return self._num_dimensions

    @abstractmethod
    def distance_between(
        self,
        point1: npt.NDArray[np.floating],
        point2: npt.NDArray[np.floating]
    ) -> float:
        """Calculate the distance between any two points using this metric."""

    @abstractmethod
    def causal_matrix(
        self,
        coordinates: npt.NDArray[np.floating]
    ) -> npt.NDArray[np.bool_]:
        """Given a list of points get the causal matrix from this metric. The
        causal matrix is defined as:
        C := {
            1: if a < b
            0: otherwise
        }
        for all points a,b in coordinates
        """


class LorentzianMetric(Metric):
    def __init__(self, num_dimentions: int, time_coordinate: int):
        super().__init__(num_dimentions)
        self._time_coordinate = time_coordinate

    def causal_matrix(
        self,
        coordinates: npt.NDArray[np.floating]
    ) -> npt.NDArray[np.bool_]:
        num_elements = coordinates.shape[0]
        matrix = np.zeros((num_elements, num_elements), dtype=np.bool_)
        for (i, i_coord), (j, j_coord) in permutations(
                enumerate(coordinates), 2):
            i_t = i_coord[self._time_coordinate]
            j_t = j_coord[self._time_coordinate]
            matrix[i, j] = self.distance_between(
                i_coord, j_coord) < 0 and (i_t < j_t)
        return matrix


class MinkowskiMetric(LorentzianMetric):
    def __init__(self, num_dimensions: int, time_coordinate: int = 0):
        super().__init__(num_dimensions, time_coordinate)
        diagonals = np.ones(num_dimensions, dtype=np.int8)
        diagonals[time_coordinate] = -1
        self._metric = np.diag(diagonals)

    def distance_between(
        self,
        point1: npt.NDArray[np.floating],
        point2: npt.NDArray[np.floating]
    ) -> float:
        vector = point1 - point2
        return float(vector @ self._metric @ vector)
