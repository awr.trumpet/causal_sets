# Welcome to the causal set package
## This package contains classes and functions to create arbirary dimensional spacetimes and run simulations over them.

### Installing the package
To install this package it's best practice to use a virtual environment. Create and activate your new virtual environment by running:
```sh
virtualenv .venv
. .venv/bin/activate
```
and you can now install the package using:

```sh
pip install causal_sets
```
or if you want to visualise the spacetimes then the additional plotting extras can be installed by:
```sh
pip install casual_sets[plotting]
```

### How to use the package
Some examples of how to use the package can be found in the notebooks directory and come in the form of Jupyter notebooks. To run the notebook you require the jupyter package to be install which is included in the plotting extras for this package. Once installed simply run
```sh
jupyter lab
```
which will open a new browser window and from there you can navigate to the directory containing the notebooks and open them.
